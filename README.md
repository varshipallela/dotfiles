# dotfiles

> My dotfiles - enjoy!

## Install

```sh
dotstow stow <SOME_PACKAGE>
```

You can find dotstow at [gitlab.com/bitspur/community/dotstow](https://gitlab.com/bitspur/community/dotstow).
